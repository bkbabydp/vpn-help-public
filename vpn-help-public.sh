#!/usr/bin/env bash

shopt -s extglob
set -o errtrace
#set -o errexit

println_red()
{
    echo -e "\033[;31;1m"$1"\033[0m"
}

print_red()
{
    echo -en "\033[;31;1m"$1"\033[0m"
}

println_green()
{
    echo -e "\033[;32;1m"$1"\033[0m"
}

print_green()
{
    echo -en "\033[;32;1m"$1"\033[0m"
}

println_yellow()
{
    echo -e "\033[;33;1m"$1"\033[0m"
}

print_yellow()
{
    echo -en "\033[;33;1m"$1"\033[0m"
}

check_yum_remove()
{
    local key="clean_requirements_on_remove"
    local value="1"
    local cmd=$key"="$value
    local file=/etc/yum.conf

    print_yellow $key
    if [ "`cat $file | grep -c $cmd`" != 0 ]; then
        println_green " OK"
        return 1
    else
        println_red " NO"
        return 0
    fi
}

do_yum_remove()
{
    check_yum_remove
    if [ $? == 0 ]; then
        echo "adding..."
        cat >> /etc/yum.conf <<EOF
clean_requirements_on_remove=1
EOF
        check_yum_remove
    fi
}

do_yum_install()
{
    yum update -y
    yum install epel-release -y
    yum install bash-completion git expect vim -y
}

do_fail2ban()
{
    yum install fail2ban -y

    cat > /etc/fail2ban/jail.d/sshd.local <<EOF
[DEFAULT]
bantime = 1209600
findtime = 3600
maxretry = 3
[sshd]
enabled = true
EOF

#firewall-cmd --permanent --add-service=http
#firewall-cmd --permanent --add-service=https
#firewall-cmd --permanent --add-service=radius
#firewall-cmd --permanent --add-port=8388/tcp #shadowsocks-libev
#firewall-cmd --reload

    systemctl enable firewalld.service
    systemctl restart firewalld.service
    systemctl status firewalld.service
    systemctl enable fail2ban.service
    systemctl restart fail2ban.service
    systemctl status fail2ban.service
}

check_sshkey()
{
    local file=$HOME/.ssh/id_rsa.pub

    print_yellow "ssh key"
    if [ -f $file ]; then
        println_green " OK"
        return 1
    else
        println_red " NO"
        return 0
    fi
}

do_sshkey()
{
    check_sshkey
    if [ $? == 0 ]; then
        echo "adding..."
        expect -c "
spawn ssh-keygen
expect {
    \"yes/no\" { send \"yes\n\"; exp_continue }
    \"no/yes\" { send \"yes\n\"; exp_continue }
    \"y/n\" { send \"y\n\"; exp_continue }
    \"n/y\" { send \"y\n\"; exp_continue }
    \"assword\" {send \"\n\"; exp_continue }
    \":\" {send \"\n\"; exp_continue }
    eof
}
"
        check_sshkey
    fi
}

menu()
{
    PS3='Please enter your choice: '
    options=("do_yum_remove" "check_yum_remove" "do_yum_install" "do_fail2ban" "do_sshkey" "check_sshkey")
    select opt in "${options[@]}"
    do
        case $opt in
            "do_yum_remove")
                do_yum_remove
                ;;
            "check_yum_remove")
                check_yum_remove
                ;;
            "do_yum_install")
                do_yum_install
                ;;
            "do_fail2ban")
                do_fail2ban
                ;;
            "do_sshkey")
                do_sshkey
                ;;
            "check_sshkey")
                check_sshkey
                ;;
            *)
                break
                ;;
        esac
    done
}

do_all()
{
    do_yum_remove
    do_yum_install
    do_fail2ban
    do_sshkey
}

do_all
