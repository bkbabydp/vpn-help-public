#!/usr/bin/env bash

shopt -s extglob
set -o errtrace
set -o errexit

ECHO_STYLE_DEFAULT="\033[0m"
ECHO_STYLE_YELLOWFONT="\033[;33;1m"

ssh_key_file=~/.ssh/id_rsa.pub
yum_conf_file=/etc/yum.conf
yum_conf_key="clean_requirements_on_remove=1"

vpn_help_git="git@bitbucket.org:bkbabydp/vpn-help.git"
vpn_help_path=~/.vpn_help
vpn_help_sh=$vpn_help_path/init/init.sh
vpn_help_lnk=/bin/vpn-help
vpn_installer_sh=$vpn_help_path/install_vpn.sh

func_show_file()
{
    echo
    cat $1
    echo
}

func_show_yellow()
{
    echo -e ${ECHO_STYLE_YELLOWFONT}$1${ECHO_STYLE_DEFAULT}
}

func_show_yellow_noline()
{
    echo -en ${ECHO_STYLE_YELLOWFONT}$1${ECHO_STYLE_DEFAULT}
}



#1
init_wait_modify_yum_conf()
{
    if [ "`cat $yum_conf_file | grep -c $yum_conf_key`" != 0 ]; then
        func_show_yellow $yum_conf_file" has "$yum_conf_key
    else
        func_show_yellow "add "$yum_conf_key" to "$yum_conf_file
        echo $yum_conf_key >> $yum_conf_file
    fi

    func_show_file $yum_conf_file
    func_show_yellow_noline "have you check the yum.conf?(y/n)"
}

#2
init_install_package()
{
    yum update -y
    yum install bash-completion git expect vim fail2ban -y
}

#3
init_wait_create_ssh_key()
{
    if [ -f $ssh_key_file ]; then
        func_show_yellow "has "$ssh_key_file
    else
        func_show_yellow "create "$ssh_key_file

        expect -c "
        spawn ssh-keygen
        expect {
            \"yes/no\" { send \"yes\n\"; exp_continue }
            \"no/yes\" { send \"yes\n\"; exp_continue }
            \"y/n\" { send \"y\n\"; exp_continue }
            \"n/y\" { send \"y\n\"; exp_continue }
            \"assword\" {send \"\n\"; exp_continue }
            \":\" {send \"\n\"; exp_continue }
            eof
        }
        "
    fi

    func_show_file $ssh_key_file
    func_show_yellow_noline "have you copy and set the key?(y/n)"
}

#4
init_vpn_help_update()
{
    mkdir -p /data
    if [ -d $vpn_help_path/.git ]; then
        func_show_yellow $vpn_help_path" is here, update it..."
        cd $vpn_help_path
        git pull --rebase
        git submodule update
    else
        func_show_yellow $vpn_help_path" not here, download it..."
        git clone $vpn_help_git $vpn_help_path
        cd $vpn_help_path
        git submodule init
        git submodule update
    fi
}

#5
init_vpn_help_lnk()
{
    if [ -f $vpn_help_lnk ]; then
        func_show_yellow $vpn_help_lnk" is here."
        ls -la $vpn_help_lnk
    else
        func_show_yellow $vpn_help_lnk" is not here."
    fi
    func_show_yellow "link "$vpn_help_lnk" with "$vpn_help_sh
    ln -sf $vpn_help_sh $vpn_help_lnk
    chmod 755 $vpn_help_sh
    chmod 755 $vpn_help_lnk
    chmod 755 $vpn_installer_sh
    ls -la $vpn_installer_sh
    ls -la $vpn_help_sh
    ls -la $vpn_help_lnk
}

init_wait_modify_yum_conf
read _KEY
case $_KEY in
    (y)
        init_install_package
        init_wait_create_ssh_key
        read _KEY
        case $_KEY in
            (y)
                init_vpn_help_update
                init_vpn_help_lnk
                ;;
            (*)
                exit 0
                ;;
        esac
        ;;
    (*)
        exit 0
        ;;
esac
