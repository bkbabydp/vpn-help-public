## Add another VPNClient

### 1. Create New Droplet

### 2. Download `vpn-help-public.sh` and install

```bash
curl -O "http://bitbucket.org/bkbabydp/vpn-help-public/raw/master/vpn-help-public.sh"
bash vpn-help-public.sh

# OR

curl -sSL "http://bitbucket.org/bkbabydp/vpn-help-public/raw/master/vpn-help-public.sh" | bash -s stable
```

**DO REMEMBER** to copy the private key to **`BOTH` [vpn-help-public](/bkbabydp/vpn-help-public) `AND` [vpn-help](/bkbabydp/vpn-help)**

### 3. Install the VPNServer

```bash
~/.vpn_help/install_vpn.sh
```

### 4. Add NAS

Add the new **`Client IP`** into the `nas` table of Radius Server's database

Example (`Postgresql`):
```sql
INSERT INTO public.nas (nasname, shortname, type, ports, secret, server, community, description) VALUES (<CLIENT IP>, 'local', 'other', null, 'radiuspwd', null, null, null);
```

And then restart the `Radius` Server

```bash
systemctl restart radiusd.service
```

## Some useful tips

* Check the `Login Log`

```bash
# show the Last Logged In Users
last

# show the Bad Login Attemps Log
lastb

# clean the Login Log
> /var/log/wtmp
> /var/log/btmp
```

* and
